use super::Db;
use crate::models::thread::{NewThread, Thread};
use crate::schema::threads;
use diesel::prelude::*;
use rocket::response::{status::Created, Debug};
use rocket::serde::json::Json;

type Result<T, E = Debug<diesel::result::Error>> = std::result::Result<T, E>;

pub fn routes() -> Vec<rocket::Route> {
    routes![create, list]
}

#[post("/index", data = "<post>")]
async fn create(db: Db, post: Json<NewThread>) -> Result<Created<Json<NewThread>>> {
    let post_value = post.clone();
    db.run(move |conn| {
        diesel::insert_into(threads::table)
            .values(post_value)
            .execute(conn)
    })
    .await?;

    Ok(Created::new("/").body(post))
}

#[get("/index")]
async fn list(db: Db) -> Result<Json<Vec<Option<i32>>>> {
    let ids: Vec<Option<i32>> = db
        .run(move |conn| threads::table.select(threads::id).load(conn))
        .await?;

    Ok(Json(ids))
}

pub async fn get_threads(db: Db) -> Result<Vec<Thread>> {
    let result = db
        .run(move |conn| {
            threads::table
                .load::<Thread>(conn)
                .expect("Could not load post")
        })
        .await;

    Ok(result)
}

pub async fn create_thread(db: Db, post: NewThread) -> Result<Created<Json<NewThread>>> {
    let post_value = post.clone();
    db.run(move |conn| {
        diesel::insert_into(threads::table)
            .values(post_value)
            .execute(conn)
    })
    .await?;

    Ok(Created::new("/").body(Json(post)))
}
