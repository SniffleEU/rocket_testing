#[macro_use]
extern crate rocket;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate diesel;

extern crate dotenv;

mod db;
mod models;
mod routes;
mod schema;

use rocket_dyn_templates::Template;

#[launch]
fn rocket() -> _ {
    rocket::build()
        .attach(Template::fairing())
        .attach(db::stage())
        .mount("/", routes::view_controller::routes())
        .register("/", catchers![routes::view_controller::not_found])
}
