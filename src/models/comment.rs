use chrono::prelude::*;
use rocket::serde::Serialize;

#[derive(Serialize, Debug, Clone)]
#[serde(crate = "rocket::serde")]
pub struct Comment {
    id: i32,
    thread_id: i32,
    created_at: DateTime<Utc>,
    title: String,
    body: String,
    //Placeholder image for later
    //Just to have data for building thread looks
    image: String,
}

impl Comment {
    pub fn fake_comment(id: i32) -> Comment {
        Comment {
            id,
            thread_id: 1,
            created_at: Utc::now(),
            title: String::from("Comment title"),
            body: String::from("Comment body"),
            image: String::from("Not implemented image"),
        }
    }
}
