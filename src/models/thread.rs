use crate::schema::threads;
use chrono::prelude::*;
use diesel::{Insertable, Queryable};
use rocket::serde::{Deserialize, Serialize};
use rocket_sync_db_pools::diesel;

#[derive(Debug, Clone, Deserialize, Serialize, Queryable)]
#[serde(crate = "rocket::serde")]
pub struct Thread {
    pub id: Option<i32>,
    pub title: Option<String>,
    pub name: Option<String>,
    pub body: String,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Debug, Clone, Deserialize, Serialize, Insertable)]
#[serde(crate = "rocket::serde")]
#[table_name = "threads"]
pub struct NewThread {
    pub title: Option<String>,
    pub name: Option<String>,
    pub body: String,
}

impl Thread {
    // pub fn fake_thread() -> Thread {
    //     Thread {
    //         id: 1,
    //         created_at: Local::now().naive_local(),
    //         name: Some(String::from("")),
    //         title: Some(String::from("Title of thread if it exists")),
    //         body: String::from("This is the thread body"),
    //     }
    // }
}
