use crate::db::threads;
use crate::db::Db;
use crate::models::thread::NewThread;
use rocket::response::{status::Created, Debug};
use rocket::serde::json::Json;
use rocket::Request;
use rocket_dyn_templates::Template;

type Result<T, E = Debug<diesel::result::Error>> = std::result::Result<T, E>;

pub fn routes() -> Vec<rocket::Route> {
    routes![index, show, create_thread]
}

#[get("/")]
pub async fn index(db: Db) -> Template {
    let threads = threads::get_threads(db)
        .await
        .expect("This is just to test pls dont fail");
    Template::render("index", threads)
}

#[get("/bye")]
pub fn show() -> &'static str {
    "Goodbye, world!"
}

#[get("/<title>/<name>/<body>")]
pub async fn create_thread(
    db: Db,
    title: Option<String>,
    name: Option<String>,
    body: String,
) -> Result<Created<Json<NewThread>>> {
    let post = NewThread { title, name, body };
    threads::create_thread(db, post).await
}

#[catch(404)]
pub fn not_found(req: &Request<'_>) -> Template {
    Template::render("error/404", req.uri())
}
