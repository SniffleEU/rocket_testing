table! {
    comments (id) {
        id -> Nullable<Integer>,
        name -> Nullable<Text>,
        title -> Nullable<Text>,
        body -> Text,
        created_at -> Timestamp,
        thread_id -> Nullable<Integer>,
    }
}

table! {
    threads (id) {
        id -> Nullable<Integer>,
        title -> Nullable<Text>,
        name -> Nullable<Text>,
        body -> Text,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

joinable!(comments -> threads (thread_id));

allow_tables_to_appear_in_same_query!(
    comments,
    threads,
);
